/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.itaihanski.triangleview.sample.slice;

import com.itaihanski.triangleview.TriangleView;
import com.itaihanski.triangleview.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;

/**
 * MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final int COLOR_YELLOW = 0xFFFFEB3B;
    private static final int VIEW_WIDTH = 400;
    private static final int VIEW_HEIGHT = 400;
    private ComponentContainer rootLayout = null;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_main_layout, null, false) instanceof ComponentContainer) {
            rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_main_layout, null, false);
        }

        // direction and color can be set in code as well as in XML attributes
        TriangleView triangleView = new TriangleView(this);
        triangleView.setDirection(TriangleView.Direction.DOWN);
        triangleView.setColor(COLOR_YELLOW);

        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig();
        layoutConfig.width = VIEW_WIDTH;
        layoutConfig.height = VIEW_HEIGHT;
        layoutConfig.alignment = DirectionalLayout.HORIZONTAL;
        triangleView.setLayoutConfig(layoutConfig);

        rootLayout.addComponent(triangleView);

        setUIContent(rootLayout);
        super.setUIContent(rootLayout);
    }

    @Override
    protected void onActive() {
        super.onActive();
    }
}
