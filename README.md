# triangle-view

A simple openharmony view to easily draw triangle shapes.
Add a ** triangle-view** to your slice the same way you'd add any other component.
The size and shape of the triangle is determined by the size given to the component.
The direction and color of the triangle is set via public setters or via the XML attributes:

# triangle-view includes :
*Set Direction
*Set Color

* Adding triangle-view programmatically
* Adding triangle-view through xml layout
* Changing color and direction of triangle using public setter and from directly from xml

# Usage Instructions

1. The related classes are as follows:

        TriangleView triangleView = new TriangleView(this);
        triangleView.setDirection(TriangleView.Direction.DOWN);
        triangleView.setColor(COLOR_YELLOW);

2. XML
            <com.itaihanski.triangleview.TriangleView
                ohos:id="$+id:1_one"
                ohos:width="50vp"
                ohos:height="50vp"
                ohos:top_margin="15vp"
                ohos:tr_direction="up"
                ohos:tr_color="#F44336"/>


# Installation Instructions

1. For using triangle-view module in sample app,include the below library jar/har in libs folder of "entry" module to generate hap/triangleview.har:
   Modify entry build.gradle as below :
        dependencies {
                  implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
        }

2. For using triangle-view in separate application make sure to add the below dependent libraries in entry gradle dependency:
   Modify entry build.gradle as below :
    	dependencies {
                  implementation project(':triangleview')
         }

3. For using triangle-view from a remote repository in separate application, add the below dependencies:
    Modify entry build.gradle as below :
    	dependencies {
    	    implementation 'io.openharmony.tpc.thirdlib:triangle-view:1.0.1'
    	}
    	
	
**Installation2**  
```gradle  
For using the picasso in sample app, Add below dependencies in gradle.

Solution 1: local har package integration
Add the .har package to the lib folder.
Add the following code to the gradle of the entry:
implementation fileTree(dir: 'libs', include: ['.jar', '.har'])

Solution 2: Add following dependencies in your build.gradle:

allprojects {
    repositories {
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:triangle-view:1.0.1'
```  
    	